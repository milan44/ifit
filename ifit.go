package ifit

import (
	"github.com/nfnt/resize"
	"image"
	"image/draw"
	"math"
)

type SizeData struct {
	Width  int
	Height int
}

type Alignment int

const (
	AlignCenter      Alignment = iota
	AlignTopLeft     Alignment = iota
	AlignBottomRight Alignment = iota
)

func GetImageData(img image.Image) SizeData {
	box := img.Bounds()
	return SizeData{
		Width:  box.Max.X,
		Height: box.Max.Y,
	}
}

func FitInto(img image.Image, fit SizeData, align Alignment) image.Image {
	data := GetImageData(img)

	if data.Height == fit.Height && data.Width == fit.Width {
		return img
	}

	final := data.GetFit(img, fit)

	resized := resize.Resize(uint(final.Width), uint(final.Height), img, resize.Lanczos3)

	position := image.Point{}
	if align == AlignTopLeft {
		position.X = 0
		position.Y = 0
	} else if align == AlignBottomRight {
		position.X = -(final.Width - fit.Width)
		position.Y = -(final.Height - fit.Height)
	} else if align == AlignCenter {
		if final.Width == fit.Width {
			h := (final.Height - fit.Height) / 2
			position.Y = -h
		} else {
			w := (final.Width - fit.Width) / 2
			position.X = -w
		}
	}

	result := image.NewRGBA(fit.Rect(0, 0))
	draw.Draw(result, final.Rect(position.X, position.Y), resized, image.Point{}, draw.Src)

	return result
}

func (s SizeData) GetFit(img image.Image, fit SizeData) SizeData {
	data := GetImageData(img)

	if data.Height == fit.Height && data.Width == fit.Width {
		return data
	}

	final := ScaleHeight(data, fit.Height)
	if final.Width < fit.Width {
		final = ScaleWidth(final, fit.Width)
	}

	return final
}

func (s SizeData) Rect(minX, minY int) image.Rectangle {
	return image.Rectangle{
		Min: image.Point{
			X: minX,
			Y: minY,
		},
		Max: image.Point{
			X: s.Width + minX,
			Y: s.Height + minY,
		},
	}
}

func ScaleWidth(s SizeData, w int) SizeData {
	if w == s.Width {
		return s
	}

	percent := float64(w) / float64(s.Width)
	s.Height = int(math.Floor(float64(s.Height) * percent))
	s.Width = w

	return s
}

func ScaleHeight(s SizeData, h int) SizeData {
	if h == s.Height {
		return s
	}

	percent := float64(h) / float64(s.Height)
	s.Width = int(math.Floor(float64(s.Width) * percent))
	s.Height = h

	return s
}
