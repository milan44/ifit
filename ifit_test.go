package ifit

import (
	"fmt"
	"testing"
)

func TestFitInto(t *testing.T) {
	// One Smaller
	doTest(SizeData{512, 256}, "height", AlignCenter)
	doTest(SizeData{256, 512}, "width", AlignCenter)

	// Both Smaller
	doTest(SizeData{256, 128}, "height", AlignCenter)
	doTest(SizeData{128, 256}, "width", AlignCenter)

	// One Bigger
	doTest(SizeData{1024, 512}, "height", AlignCenter)
	doTest(SizeData{512, 1024}, "width", AlignCenter)

	// Both Bigger
	doTest(SizeData{2048, 1024}, "height", AlignCenter)
	doTest(SizeData{1024, 2048}, "width", AlignCenter)

	// Other Alignments
	doTest(SizeData{512, 256}, "top", AlignTopLeft)
	doTest(SizeData{512, 256}, "bottom", AlignBottomRight)
}

func doTest(s SizeData, f string, a Alignment) {
	example, err := LoadImageFile("./test/smaller_" + f + ".png")
	must(err)

	result := FitInto(example, s, a)

	err = SaveImageFile(result, "out_"+fmt.Sprint(s.Width, "x", s.Height)+"_"+f+".png")
	must(err)
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
